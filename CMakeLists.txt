## automatically generated CMakeLists.txt file

cmake_minimum_required(VERSION 3.0)

# Declare the package
atlas_subdir( BLSToolBox )

# Declare external dependencies ... default here is to include ROOT
find_package( ROOT COMPONENTS MathCore RIO Core Tree Hist )

# Declare public and private dependencies
# Public dependencies are anything that appears in the headers in public include dir
# Private is anything else

# An example is included
atlas_depends_on_subdirs(
    PUBLIC

    PRIVATE
    Control/AthAnalysisBaseComps
)

# Declare package as a library
# Note the convention that library names get "Lib" suffix
# Any package you add to dependencies above, you should add
# to LINK_LIBRARIES line below (see the example)
atlas_add_library( BLSToolBoxLib src/*.cxx Root/*.cxx
                   PUBLIC_HEADERS BLSToolBox
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
                                    AthAnalysisBaseCompsLib xAODEventInfo
                                    xAODRootAccess xAODBase xAODCore xAODEgamma xAODTracking 
                                    xAODMuon xAODBPhysLib
                                    xAODJet
                                    TrigDecisionTool TriggerMatchingTool
                                    InDetTrackSelectionTool TrigDecisionToolLib TrigConfInterfaces
                                    AsgTools
                                    MuonMomentumCorrectionsLib MuonSelectorToolsLib MuonAnalysisInterfacesLib
				    MuonEfficiencyCorrectionsLib
                                    EgammaAnalysisInterfacesLib ElectronPhotonSelectorToolsLib
                                    xAODParticleEvent PileupReweightingLib
                )


# if you add components (tools, algorithms) to this package
# these lines are needed so you can configure them in joboptions
atlas_add_component( BLSToolBox src/*.cxx Root/*.cxx src/components/*.cxx
                      LINK_LIBRARIES BLSToolBoxLib 
)

# if you add an application (exe) to this package
# declare it like this (note convention that apps go in the util dir)
# atlas_add_executable( MyApp util/myApp.cxx
#                       LINK_LIBRARIES BLSToolBoxLib
# )

# Install python modules, joboptions, and share content
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )
atlas_install_data( data/* )
# You can access your data from code using path resolver, e.g.
# PathResolverFindCalibFile("BLSToolBox/file.txt")



