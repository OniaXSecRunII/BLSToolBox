// BLSToolBox includes
#include "BLSToolBoxAlg.h"

//#include "xAODEventInfo/EventInfo.h"




BLSToolBoxAlg::BLSToolBoxAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ),
m_toolbox("BLToolBox/ToolBox")
{
    
    m_toolbox.declarePropertyFor( this, "ToolBox" , "Configured toolbox" ); //example property declaration
    //declareProperty( "ToolBox", m_toolbox, "My Example Integer Property" ); //example property declaration

}


BLSToolBoxAlg::~BLSToolBoxAlg() {}


StatusCode BLSToolBoxAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory


    m_toolbox.setTypeAndName("BLS::BLSToolBox/BLSToolBox");
    //ATH_CHECK( m_toolbox.setProperty("WriteTruthTaus", true) );
    ATH_CHECK( m_toolbox.retrieve() );
    ATH_MSG_INFO("Retrieved tool: " << m_toolbox->name() );

    
  return StatusCode::SUCCESS;
}

StatusCode BLSToolBoxAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode BLSToolBoxAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

    auto muons = m_toolbox->muons();
    ATH_MSG_INFO("eventNumber=" << m_toolbox->eventNumber() );
    ATH_MSG_INFO("Nmuons =" << muons->size() );
    const std::string triggerName = "HLT_2mu6_bJpsimumu";
    bool passed = m_toolbox->tdt()->isPassed(triggerName);
    ATH_MSG_INFO("tdt =" << passed);
    
    for ( unsigned int imu0 =0; imu0 < muons->size(); ++imu0) {
        auto mu0       = muons->at(imu0);
        auto mu0_calib = m_toolbox->makeCalibMuon(mu0);
        bool p(false);
        ATH_MSG_INFO("Calib comp: mu:  " << mu0->pt() << " " << mu0_calib->pt() << " "
                     << mu0->eta() << " " << mu0_calib->eta() << " "
                     << (m_toolbox->passMuonTight(mu0,p)             ? p : false ) << " "
                     << (m_toolbox->passMuonTight(mu0_calib.get(),p) ? p : false )
                     );
    }
    
    if (passed) {
        for ( unsigned int imu0 =0; imu0 < muons->size(); ++imu0) {
            auto mu0 = muons->at(imu0);

            for ( unsigned int imu1 =imu0+1; imu1 < muons->size(); ++imu1) {
                auto mu1 = muons->at(imu1);
                
                //std::vector<const xAOD::IParticle*> ptls = {
                
                ATH_MSG_INFO("  Matchings =" << "Matched (0.01, 0.05) " << m_toolbox->match({mu0,mu1},triggerName, 0.01,false));
                
            }
        }
    } // passed trigger

//    auto electrons = m_toolbox->electrons();
//    ATH_MSG_INFO("Nelectrons =" << electrons->size() );
//    for (auto ele: *electrons) {
//        ATH_MSG_INFO("  Ele: " << m_toolbox->isElectronLooseLH(ele) << " "
//                     <<  m_toolbox->isElectronMediumLH(ele) << " " << m_toolbox->isElectronTightLH(ele) << " ");
//
//    } // for

    const bool useDFVariables(true);
    auto photons = m_toolbox->photons();
    ATH_MSG_INFO("Nphotons =" << photons->size() );
    for (auto pho: *photons) {
        float val(-1);
        m_toolbox->isolation(val, pho, xAOD::Iso::ptcone20);
        ATH_MSG_INFO("  Pho: " << pho->pt() << " "
                     << m_toolbox->isPhotonLoose(pho,useDFVariables) << " "
                     << m_toolbox->isPhotonTight(pho,useDFVariables) << " "
                     << "ptcone20: " << val);
   
        
        
    } // for

    
    
    
//    auto copyMuons = m_toolbox->makeShallow<xAOD::MuonContainer>(muons);
//    ATH_MSG_INFO("Nmuons copy =" << copyMuons.first->size() );
//
//    delete copyMuons.first;
//    delete copyMuons.second;
//    

  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode BLSToolBoxAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


