
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../BLSToolBoxAlg.h"
#include "BLSToolBox/BLSToolBox.h"

DECLARE_ALGORITHM_FACTORY( BLSToolBoxAlg )
DECLARE_NAMESPACE_TOOL_FACTORY(BLS, BLSToolBox )

DECLARE_FACTORY_ENTRIES( BLSToolBox ) 
{
  DECLARE_ALGORITHM( BLSToolBoxAlg );
  DECLARE_NAMESPACE_TOOL( BLS, BLSToolBox    );
}
