#ifndef BLS_IBLSTOOLBOXTOOL_H
#define BLS_IBLSTOOLBOXTOOL_H 1

#include "AsgTools/IAsgTool.h"
#include "xAODBase/IParticle.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include <AsgTools/MessageCheck.h>

#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODEgamma/EgammaContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"

#include "xAODJet/JetContainer.h"
#include <xAODCore/ShallowCopy.h>
#include "AthContainers/ConstDataVector.h"

#include "xAODBPhys/BPhysHelper.h"
#include "xAODBPhys/BPhysHypoHelper.h"

#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "TriggerMatchingTool/IMatchingTool.h"

#include "xAODEgamma/EgammaDefs.h"
#include "xAODPrimitives/IsolationType.h"
#include "xAODBase/ObjectType.h"
#include "PATInterfaces/CorrectionCode.h"

#include "EgammaAnalysisInterfaces/IAsgDeadHVCellRemovalTool.h"
#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgEGammaIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"

#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"

#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "MuonAnalysisInterfaces/IMuonTriggerScaleFactors.h"

#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "TLorentzVector.h"

#include <iostream>
#include <tuple>
#include <memory>

#include "CxxUtils/make_unique.h"
#include "PATCore/TAccept.h"

namespace xAOD {
}

namespace BLS {

    enum class BLSQuality {VeryLoose, Loose, Medium, Tight, LowPt, NominalQuality, DummyQuality};


    class IBLSToolBox: public virtual asg::IAsgTool {
        public:
        ASG_TOOL_INTERFACE( IBLSToolBox ) //declares the interface to athena


        // Common interface for all tools in this package;
        virtual StatusCode initialize() = 0;

        // event level info
        virtual const xAOD::EventInfo* eventInfo() const = 0;
        virtual unsigned int runNumber          () const = 0;
        virtual unsigned int eventNumber        () const = 0;
        virtual unsigned int lumiBlock          () const = 0;

        virtual bool     isMC()      const = 0;
        virtual uint32_t timeStamp() const = 0;
        virtual uint32_t bcid()      const = 0;
        virtual uint32_t mcChannelNumber()         const = 0;
        virtual unsigned long long mcEventNumber() const = 0;

        virtual float actualInteractionsPerCrossing () const = 0;
        virtual float averageInteractionsPerCrossing() const = 0;
        // mu

        // PileUpReweighting
        virtual  CP::IPileupReweightingTool* prwTool()  = 0;
        virtual const CP::IPileupReweightingTool* prwTool() const = 0;

        // beam pos
        //virtual std::tuple<double,double,double> getBeamPos() const = 0;
        virtual StatusCode getBeamPos(double &x, double &y, double &z) const = 0;
        virtual StatusCode getBeamSig(double &x, double &y, double &z) const = 0;

        ///Assuming run number is data, get the corresponding year
        ///If out of range; assume in latest year, else throw exception ...
        ///return in form YYYY
        virtual int getYear(uint32_t runNumber) const = 0;


        // containers -- original
        virtual const xAOD::MuonContainer         * muons    () const = 0;
        virtual const xAOD::TrackParticleContainer* tracks   () const = 0;
        virtual const xAOD::VertexContainer       * pvs      () const = 0;
        virtual const xAOD::ElectronContainer     * electrons() const = 0;
        virtual const xAOD::PhotonContainer       * photons  () const = 0;
        virtual const xAOD::JetContainer          * jets     () const = 0;
        virtual const xAOD::JetContainer          * jets     (const std::string & key) const = 0;

        virtual const xAOD::VertexContainer       * vertices (const std::string & key) const = 0;


        virtual const xAOD::TruthEventContainer   * truthEvents   () const = 0;
        virtual const xAOD::TruthParticleContainer* truthParticles() const = 0;
        virtual const xAOD::TruthVertexContainer  * truthVertices () const = 0;


        //muons
        virtual StatusCode passMuon      (const xAOD::Muon*, BLSQuality, bool & isPassed) const = 0;

        virtual StatusCode passMuon      (const xAOD::Muon*, xAOD::Muon::Quality, bool & isPassed) const = 0;
        virtual StatusCode passMuonLoose (const xAOD::Muon*, bool& isPassed) const = 0;
        virtual StatusCode passMuonMedium(const xAOD::Muon*, bool& isPassed) const = 0;
        virtual StatusCode passMuonTight (const xAOD::Muon*, bool& isPassed) const = 0;
        virtual StatusCode passMuonLowPt (const xAOD::Muon*, bool& isPassed) const = 0;

        virtual std::unique_ptr<xAOD::Muon> makeCalibMuon(const xAOD::Muon*) const = 0;

        virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFTightTool()  const = 0;
        virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFMediumTool() const = 0;
        virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFLooseTool()  const = 0;
        virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFLowPtTool()  const = 0;

        virtual CP::CorrectionCode getMCPEfficiency(const CP::IMuonEfficiencyScaleFactors* muEffSFTool, const xAOD::Muon& mu, float& eff, const xAOD::EventInfo* info, bool isMC) const = 0;

        virtual const CP::IMuonTriggerScaleFactors* muonTriggSFTightTool()  const = 0;
        virtual const CP::IMuonTriggerScaleFactors* muonTriggSFMediumTool() const = 0;
        virtual const CP::IMuonTriggerScaleFactors* muonTriggSFLooseTool()  const = 0;

        // trigger
        virtual const Trig::TrigDecisionTool* tdt() const = 0;
        virtual bool isPassed(const std::string&) const = 0;

        virtual const Trig::IMatchingTool* trigMatchingTool() const = 0;
        virtual bool match(const xAOD::IParticle& recoObject, const std::string& chain, double matchThreshold=0.1, bool rerun=false) = 0;
        virtual bool match(const std::vector<const xAOD::IParticle*>& recoObjects, const std::string& chain, double matchThreshold=0.1, bool rerun=false) = 0;

        // electrons
        virtual Root::TAccept isElectronMediumLH(const xAOD::Electron*) const = 0;
        virtual Root::TAccept isElectronLooseLH (const xAOD::Electron*) const = 0;
        virtual Root::TAccept isElectronTightLH (const xAOD::Electron*) const = 0;

        // photons
        virtual Root::TAccept  isPhotonLoose (const xAOD::Photon*,bool useDerivationValues) const = 0;
        virtual Root::TAccept  isPhotonTight (const xAOD::Photon*,bool useDerivationValues) const = 0;


        virtual bool isolation(float& value, const xAOD::IParticle*, xAOD::Iso::IsolationType) const =0;

        // counters for quick cutflows
        virtual const std::map<TString, Long64_t>& counter() const  = 0;
        virtual void add_count   (const TString&)           = 0;
        virtual void dump_counter(const std::string &prefix) const = 0;


        // bphys helper
        virtual std::unique_ptr<xAOD::BPhysHelper>     makeBPhys    (const xAOD::Vertex*) const = 0;
        virtual std::unique_ptr<xAOD::BPhysHypoHelper> makeBPhysHypo(const std::string &, const xAOD::Vertex*) const = 0;

        //        template <typename T>
        //        std::pair<T*, xAOD::ShallowAuxContainer*> makeShallow(const T*);



        //        template <typename T>
        //        CxxUtils::unique_ptr<ConstDataVector<T> > makeViewContainer(const T*);


        // photons


        // ANGLES
        virtual bool CSFrame(const TLorentzVector & Z, TVector3 &CSAxis,
                     TVector3 &xAxis, TVector3 &yAxis, bool qDirOK = true,
                     double beamEnergy = 6500000.) const = 0;

        virtual double getCosTheta_direct(const TLorentzVector & Z,
                                  const TLorentzVector & muM,
                                  const TLorentzVector & muP, bool qDirOK = true) const = 0;

        virtual double getPhi(const TLorentzVector & Z,
                      const TLorentzVector & muM,
                      const TLorentzVector & muP,  bool qDirOK = true) const = 0;

        virtual double  getCosTheta(const TLorentzVector & Z,
                            const TLorentzVector & muM,
                            const TLorentzVector & muP,  bool qDirOK = true) const = 0;


        virtual bool VatoCS(const TLorentzVector & mupl,
                            const TLorentzVector & mumi,
                            const TLorentzVector & gamma,
                            TVector3 & CSAxis,
                            TVector3 & xAxis,
                            TVector3 & yAxis,
                            double & cosThetaCS_dimu,
                            double & cosThetaCS_gamma,
                            double & phiCS_dimu,
                            double & phiCS_gamma) const = 0;
        virtual  bool VatoCS(const TLorentzVector & mupl,
                             const TLorentzVector & mumi,
                             TVector3 & CSAxis,
                             TVector3 & xAxis,
                             TVector3 & yAxis,
                             double & cosTheta_mupl,
                             double & cosTheta_mumi,
                             double & phi_mupl,
                             double & phi_mumi) const = 0;

        virtual  bool VatoHelicity(const TLorentzVector & mupl,
                                  const TLorentzVector & mumi,
                                  TVector3 & HelicityAxis,
                                  TVector3 & xAxis,
                                  TVector3 & yAxis,
                                  double & cosTheta_mupl,
                                  double & cosTheta_mumi,
                                  double & phi_mupl,
                                  double & phi_mumi) const = 0;

        virtual  bool VatoHelicity(const TLorentzVector & mupl, const TLorentzVector & mumi,
                                   double & cosTheta_mupl,double & phi_mupl) const = 0; // shorthand of above

        virtual bool VatoCS(const TLorentzVector & mupl, const TLorentzVector & mumi, const TLorentzVector & gamma,
                            double & cosThetaCS_dimu, double & phiCS_dimu) const = 0; // shorthand of above

        virtual  bool VatoCS(const TLorentzVector & mupl, const TLorentzVector & mumi,
                             double & cosTheta_mupl,double & phi_mupl) const = 0; // shorthand of above


        // standard
        virtual double cosMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const = 0;
        virtual double phiMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const = 0;



        // Truth
        virtual const xAOD::TruthParticle * productionParticle(const xAOD::TruthParticle*) const = 0;
        virtual const xAOD::TruthParticle * decayParticle     (const xAOD::TruthParticle*) const = 0;

        virtual const xAOD::TruthParticle * getLastBhadron(const xAOD::TruthParticle*) const = 0;

        virtual bool hasChildPdg (const xAOD::TruthParticle* mother, int pdgID, bool useAbs=false) const = 0;
        virtual bool hasParentPdg(const xAOD::TruthParticle* parent, int pdgID, bool useAbs=false) const = 0;

        virtual const xAOD::TruthParticle* getDescendentPdg (const xAOD::TruthParticle* mother, int pdgID, bool useAbs) const = 0;


        virtual bool getChildren(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> & children, int pdgID, bool useAbs=false) const = 0;
        virtual bool getParents(const xAOD::TruthParticle* child, std::vector<const xAOD::TruthParticle*> & parents, int pdgID, bool useAbs=false) const = 0;

        virtual bool getSiblings(const xAOD::TruthParticle* child,std::vector<const xAOD::TruthParticle*> & siblings) const = 0;


        virtual void stableChildren(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> & children,double minPt=500., double maxEta = 2.5) const = 0;

        virtual int getBflavour(const xAOD::TruthParticle* mother) const = 0;



    }; //IBLSToolBox

//    template <typename T>
//    std::pair<T*, xAOD::ShallowAuxContainer*> IBLSToolBox::makeShallow(const T* objs)  {
//        //https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_xaod_modify/
//        //auto shallowCopy = xAOD::shallowCopyContainer (*jets);
//        //std::unique_ptr<xAOD::JetContainer> shallowJets (shallowCopy.first);
//        //std::unique_ptr<xAOD::ShallowAuxContainer> shallowAux (shallowCopy.second);
//
//        // Important - ownership passes to the caller
//        auto shallowCopy = xAOD::shallowCopyContainer (*objs);
//        return shallowCopy;
//    }

    //    template <typename T>
    //    CxxUtils::unique_ptr<ConstDataVector<T> > IBLSToolBox::makeViewContainer() {
    //        // https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_xaod_modify/
    //        auto container = CxxUtils::make_unique<ConstDataVector<T> > (SG::VIEW_ELEMENTS);
    //        return container;
    //    }


} // namespace



#endif

