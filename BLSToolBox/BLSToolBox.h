#ifndef BLS_BLSTOOLBOXTOOL_H
#define BLS_BLSTOOLBOXTOOL_H 1

#include "AsgTools/IAsgTool.h"
#include "xAODBase/IParticle.h"
#include "AsgTools/AnaToolHandle.h"

#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h"
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "MuonAnalysisInterfaces/IMuonTriggerScaleFactors.h"

#include "TriggerMatchingTool/MatchingTool.h"
#include "PileupReweighting/PileupReweightingTool.h"



#include "BLSToolBox/IBLSToolBox.h"

#include <map>
#include <string>
#include <vector>

namespace BLS {
class BLSToolBox : public asg::AsgTool, public virtual IBLSToolBox{
    ASG_TOOL_CLASS(BLSToolBox, IBLSToolBox)

    public:

    virtual StatusCode initialize() override;
    virtual ~BLSToolBox();
    BLSToolBox(const std::string &name);

    virtual const xAOD::EventInfo* eventInfo() const override;
    virtual unsigned int runNumber    () const override;
    virtual unsigned int eventNumber  () const override;
    virtual unsigned int lumiBlock    () const override;

    virtual bool     isMC()      const override;
    virtual uint32_t timeStamp() const override;
    virtual uint32_t bcid()      const override;
    virtual uint32_t mcChannelNumber()         const override;
    virtual unsigned long long mcEventNumber() const override;

    virtual float actualInteractionsPerCrossing () const override;
    virtual float averageInteractionsPerCrossing() const override;

    virtual  CP::IPileupReweightingTool* prwTool()  override;
    virtual const CP::IPileupReweightingTool* prwTool() const override;


    //virtual std::tuple<double,double,double> getBeamPos() const override;
    virtual StatusCode getBeamPos(double &x, double &y, double &z) const override;
    virtual StatusCode getBeamSig(double &x, double &y, double &z) const override;

    virtual int getYear(uint32_t runNumber) const override;


    // muons
    virtual StatusCode passMuon      (const xAOD::Muon*, xAOD::Muon::Quality, bool & isPassed) const override;
    virtual StatusCode passMuon      (const xAOD::Muon*, BLSQuality, bool & isPassed) const override;
    virtual StatusCode passMuonLoose (const xAOD::Muon*, bool& isPassed) const override;
    virtual StatusCode passMuonMedium(const xAOD::Muon*, bool& isPassed) const override;
    virtual StatusCode passMuonTight (const xAOD::Muon*, bool& isPassed) const override;
    virtual StatusCode passMuonLowPt (const xAOD::Muon*, bool& isPassed) const override;

    virtual std::unique_ptr<xAOD::Muon> makeCalibMuon(const xAOD::Muon*) const override;

    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFTightTool()  const override;
    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFMediumTool() const override;
    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFLooseTool()  const override;
    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFLowPtTool()  const override;

    virtual CP::CorrectionCode getMCPEfficiency(const CP::IMuonEfficiencyScaleFactors* muEffSFTool, const xAOD::Muon& mu, float& eff, const xAOD::EventInfo* info, bool isMC) const override;

    virtual const CP::IMuonTriggerScaleFactors* muonTriggSFTightTool()   const override;
    virtual const CP::IMuonTriggerScaleFactors* muonTriggSFMediumTool() const override;
    virtual const CP::IMuonTriggerScaleFactors* muonTriggSFLooseTool()   const override;

    // containers
    virtual const xAOD::MuonContainer         * muons    () const override;
    virtual const xAOD::TrackParticleContainer* tracks   () const override;
    virtual const xAOD::VertexContainer       * pvs      () const override;
    virtual const xAOD::ElectronContainer     * electrons() const override;
    virtual const xAOD::PhotonContainer       * photons  () const override;
    virtual const xAOD::JetContainer          * jets     () const override; /// default collection
    virtual const xAOD::JetContainer          * jets     (const std::string & key) const override;

    virtual const xAOD::VertexContainer       * vertices (const std::string & key) const override;

    virtual const xAOD::TruthEventContainer   * truthEvents   () const override;
    virtual const xAOD::TruthParticleContainer* truthParticles() const override;
    virtual const xAOD::TruthVertexContainer  * truthVertices () const override;

    // trigger
    virtual const Trig::TrigDecisionTool* tdt() const ;
    virtual bool  isPassed(const std::string&) const override;

    virtual const Trig::IMatchingTool* trigMatchingTool() const override;
    virtual bool match(const xAOD::IParticle& recoObject, const std::string& chain, double matchThreshold=0.1, bool rerun=false) override;
    virtual bool match(const std::vector<const xAOD::IParticle*>& recoObjects, const std::string& chain, double matchThreshold=0.1, bool rerun=false) override;


    // electron
    virtual Root::TAccept isElectronMediumLH(const xAOD::Electron*) const override;
    virtual Root::TAccept isElectronLooseLH (const xAOD::Electron*) const override;
    virtual Root::TAccept isElectronTightLH (const xAOD::Electron*) const override;

    virtual Root::TAccept  isPhotonLoose (const xAOD::Photon*,bool useDerivationValues) const override;
    virtual Root::TAccept  isPhotonTight (const xAOD::Photon*,bool useDerivationValues) const override;

    virtual bool isolation(float& value, const xAOD::IParticle*, xAOD::Iso::IsolationType) const override;



    // BPhys
    virtual std::unique_ptr<xAOD::BPhysHelper> makeBPhys(const xAOD::Vertex*) const override;
    virtual std::unique_ptr<xAOD::BPhysHypoHelper> makeBPhysHypo(const std::string &, const xAOD::Vertex*) const override;


    virtual const std::map<TString, Long64_t>& counter() const  override;
    virtual void add_count   (const TString&)           override;
    virtual void dump_counter(const std::string &prefix) const override;


    // ANGLES
    virtual bool CSFrame(const TLorentzVector & Z, TVector3 &CSAxis,
                 TVector3 &xAxis, TVector3 &yAxis, bool qDirOK,
                 double beamEnergy = 6500000.) const override;

    virtual double getCosTheta_direct(const TLorentzVector & Z,
                              const TLorentzVector & muM,
                              const TLorentzVector & muP, bool qDirOK) const override;

    virtual double getPhi(const TLorentzVector & Z,
                  const TLorentzVector & muM,
                  const TLorentzVector & muP,  bool qDirOK) const override;

    virtual double  getCosTheta(const TLorentzVector & Z,
                        const TLorentzVector & muM,
                        const TLorentzVector & muP,  bool qDirOK) const override;

    virtual bool VatoCS(const TLorentzVector & mupl,
                        const TLorentzVector & mumi,
                        const TLorentzVector & gamma,
                        TVector3 & CSAxis,
                        TVector3 & xAxis,
                        TVector3 & yAxis,
                        double & cosTheta_dimu,
                        double & cosTheta_gamma,
                        double & phi_dimu,
                        double & phi_gamma) const override;
    virtual  bool VatoCS(const TLorentzVector & mupl,
                         const TLorentzVector & mumi,
                         TVector3 & CSAxis,
                         TVector3 & xAxis,
                         TVector3 & yAxis,
                         double & cosTheta_mupl,
                         double & cosTheta_mumi,
                         double & phi_mupl,
                         double & phi_mumi) const override;

    virtual  bool VatoHelicity(const TLorentzVector & mupl,
                              const TLorentzVector & mumi,
                              TVector3 & HelicityAxis,
                              TVector3 & xAxis,
                              TVector3 & yAxis,
                              double & cosTheta_mupl,
                              double & cosTheta_mumi,
                              double & phi_mupl,
                               double & phi_mumi) const override;

    virtual  bool VatoHelicity(const TLorentzVector & mupl, const TLorentzVector & mumi,
                               double & cosTheta_mupl,double & phi_mupl) const override; // shorthand of above

    virtual bool VatoCS(const TLorentzVector & mupl, const TLorentzVector & mumi, const TLorentzVector & gamma,
                        double & cosThetaCS_dimu, double & phiCS_dimu) const override; // shorthand of above

    virtual  bool VatoCS(const TLorentzVector & mupl, const TLorentzVector & mumi,
                         double & cosTheta_mupl,double & phi_mupl) const override; // shorthand of above




    // standard
    virtual double cosMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const override;
    virtual double phiMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const override;


    // TRUTH
    virtual const xAOD::TruthParticle * productionParticle(const xAOD::TruthParticle*) const override;
    virtual const xAOD::TruthParticle * decayParticle     (const xAOD::TruthParticle*) const override;

    virtual const xAOD::TruthParticle * getLastBhadron(const xAOD::TruthParticle*) const override;

    virtual bool hasChildPdg (const xAOD::TruthParticle* mother, int pdgID, bool useAbs=false) const override;
    virtual bool hasParentPdg(const xAOD::TruthParticle* parent, int pdgID, bool useAbs=false) const override;

    virtual const xAOD::TruthParticle* getDescendentPdg (const xAOD::TruthParticle* mother, int pdgID, bool useAbs) const override;


    virtual bool getChildren(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> & children, int pdgID, bool useAbs=false) const override;
    virtual bool getParents(const xAOD::TruthParticle* child, std::vector<const xAOD::TruthParticle*> & parents, int pdgID, bool useAbs=false) const override;

    virtual bool getSiblings(const xAOD::TruthParticle* child,std::vector<const xAOD::TruthParticle*> & siblings) const override;


    virtual void stableChildren(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> & children,double minPt=500., double maxEta = 2.5) const override;

    virtual int getBflavour(const xAOD::TruthParticle* mother) const override;



    private:

    // container names
    // default xAOD labels
    std::string m_muons_key          = "Muons";
    std::string m_pv_key             = "PrimaryVertices";
    std::string m_tracks_key         = "InDetTrackParticles";
    std::string m_photons_key        = "Photons";
    std::string m_electrons_key      = "Electrons";
    std::string m_truthEvents_key    = "TruthEvents";
    std::string m_truthParticles_key = "TruthParticles";
    std::string m_truthVertices_key  = "TruthVertices";
    //std::string m_truthMuons_key     = "";
    std::string m_truthEGamma_key    = "egammaTruthParticles";
    std::string m_jets_key           = "AntiKt4EMTopoJets";


    // PRW
    asg::AnaToolHandle<CP::IPileupReweightingTool> m_prw_tool;

    // MUONS

    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_loose;
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_medium;
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_tight;
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_lowpt;

    asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibSmearTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFTightTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFMediumTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFLooseTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFLowPtTool;

    asg::AnaToolHandle<CP::IMuonTriggerScaleFactors>     m_muonTriggSFTightTool;
    asg::AnaToolHandle<CP::IMuonTriggerScaleFactors>     m_muonTriggSFMediumTool;
    asg::AnaToolHandle<CP::IMuonTriggerScaleFactors>     m_muonTriggSFLooseTool;

    // TRIGGER
    asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!
    asg::AnaToolHandle<Trig::TrigDecisionTool>    m_trigDecisionTool; //!
    asg::AnaToolHandle<Trig::IMatchingTool >      m_triggerMatchingTool; //!

    // PHOTONS / ELECTRONS
    asg::AnaToolHandle<IAsgElectronLikelihoodTool>  m_electon_mediumLH; //!
    asg::AnaToolHandle<IAsgElectronLikelihoodTool>  m_electon_looseLH; //!
    asg::AnaToolHandle<IAsgElectronLikelihoodTool>  m_electon_tightLH; //!

    asg::AnaToolHandle<IAsgPhotonIsEMSelector>  m_photon_loose; //!
    asg::AnaToolHandle<IAsgPhotonIsEMSelector>  m_photon_tight; //!



    std::map<TString, Long64_t> m_counter;

};




}
#endif

