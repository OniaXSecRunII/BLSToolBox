#include "BLSToolBox/BLSToolBox.h"

#include "xAODEventInfo/EventInfo.h"
#include "CxxUtils/make_unique.h"


#include "MuonSelectorTools/MuonSelectionTool.h"


using namespace BLS;


BLSToolBox::BLSToolBox(const std::string& name): asg::AsgTool( name ),
m_prw_tool                ("CP::PileupReweightingTool/ToolBoxPileupReweightingTool"),
m_muonSelectionTool_loose ("CP::MuonSelectionTool/ToolBoxMuonSelectionToolLoose"),
m_muonSelectionTool_medium("CP::MuonSelectionTool/ToolBoxMuonSelectionToolMedium"),
m_muonSelectionTool_tight ("CP::MuonSelectionTool/ToolBoxMuonSelectionToolTight"),
m_muonSelectionTool_lowpt ("CP::MuonSelectionTool/ToolBoxMuonSelectionToolLowPt"),
m_muonCalibSmearTool      ("CP::MuonCalibrationAndSmearingTool/ToolBoxMuonCalibrationAndSmearingTool" ),
m_muonEffSFTightTool      ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsTightTool"),
m_muonEffSFMediumTool     ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsMediumTool"),
m_muonEffSFLooseTool      ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsLooseTool"),
m_muonEffSFLowPtTool      ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsLowPtTool"),
m_muonTriggSFTightTool    ("CP::MuonTriggerScaleFactors/ToolBoxMuonTriggerScaleFactorsTightTool"),
m_muonTriggSFMediumTool   ("CP::MuonTriggerScaleFactors/ToolBoxMuonTriggerScaleFactorsMediumTool"),
m_muonTriggSFLooseTool    ("CP::MuonTriggerScaleFactors/ToolBoxMuonTriggerScaleFactorsLooseTool"),
m_trigConfigTool          ("TrigConf::xAODConfigTool/xAODConfigTool"),
m_trigDecisionTool        ("Trig::TrigDecisionTool/TrigDecisionTool"),
m_triggerMatchingTool     ("Trig::MatchingTool/TriggerMatchingTool"),
m_electon_mediumLH        ("AsgElectronLikelihoodTool/ElectronSelectionMediumLH"),
m_electon_looseLH         ("AsgElectronLikelihoodTool/ElectronSelectionLooseLH"),
m_electon_tightLH         ("AsgElectronLikelihoodTool/ElectronSelectionTightLH"),
m_photon_loose            ("AsgPhotonIsEMSelector/PhotonIsEMSelectorLoose"),
m_photon_tight            ("AsgPhotonIsEMSelector/PhotonIsEMSelectorTight")
{

    m_prw_tool  .declarePropertyFor(this, "ToolBoxPileupReweightingTool","PRW tool");

    m_muonSelectionTool_loose .declarePropertyFor( this, "MuonSelectionToolLoose" , "Loose" );
    m_muonSelectionTool_medium.declarePropertyFor( this, "MuonSelectionToolMedium", "Medium");
    m_muonSelectionTool_tight .declarePropertyFor( this, "MuonSelectionToolTight" , "Tight" );
    m_muonSelectionTool_lowpt .declarePropertyFor( this, "MuonSelectionToolLowPt" , "LowPt" );

    m_muonCalibSmearTool.declarePropertyFor( this, "ToolBoxMuonCalibrationAndSmearingTool" , "calib" );

    m_muonEffSFTightTool  .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsTightTool" , "muonEffs" );
    m_muonEffSFMediumTool .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsMediumTool" , "muonEffs" );
    m_muonEffSFLooseTool  .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsLooseTool" , "muonEffs" );
    m_muonEffSFLowPtTool  .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsLowPtTool" , "muonEffs" );

    m_muonTriggSFTightTool .declarePropertyFor( this, "ToolBoxMuonTriggerScaleFactorsTightTool" , "muonTriggSf" );
    m_muonTriggSFMediumTool.declarePropertyFor( this, "ToolBoxMuonTriggerScaleFactorsMediumTool" , "muonTriggSf" );
    m_muonTriggSFLooseTool .declarePropertyFor( this, "ToolBoxMuonTriggerScaleFactorsLooseTool" , "muonTriggSf" );

    m_electon_mediumLH.declarePropertyFor( this, "ElectronSelectionMediumLH", "medium" );
    m_electon_tightLH .declarePropertyFor( this, "ElectronSelectionTightLH" , "tight" );
    m_electon_looseLH .declarePropertyFor( this, "ElectronSelectionLooseLH" , "loose" );

    m_photon_loose  .declarePropertyFor( this, "PhotonIsEMSelectorLoose", "loose" );
    m_photon_tight  .declarePropertyFor( this, "PhotonIsEMSelectorTight", "tight" );

    m_triggerMatchingTool  .declarePropertyFor( this, "TriggerMatchingTool", "trig matching" );


    declareProperty("MuonsKey",         m_muons_key);
    declareProperty("PVKey",            m_pv_key);
    declareProperty("TracksKey",        m_tracks_key);
    declareProperty("PhotonsKey",       m_photons_key);
    declareProperty("ElectronsKey",     m_electrons_key);
    declareProperty("TruthEventsKey",   m_truthEvents_key);
    declareProperty("TruthParticlesKey",m_truthParticles_key);
    declareProperty("TruthVerticesKey" ,m_truthVertices_key);
    declareProperty("TruthEGammaKey",   m_truthEGamma_key);
    declareProperty("JetsKey",          m_jets_key);


}

StatusCode BLSToolBox::initialize(){


    std::vector<std::string> listOfLumicalcFiles = {
        "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
        "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
    };
    ANA_CHECK(m_prw_tool.setProperty("DataScaleFactor",1./1.09));
    ANA_CHECK(m_prw_tool.setProperty("DataScaleFactorUP",1.));
    ANA_CHECK(m_prw_tool.setProperty("DataScaleFactorDOWN",1./1.18));
    ANA_CHECK(m_prw_tool.setProperty("LumiCalcFiles", listOfLumicalcFiles));
    ANA_CHECK(m_prw_tool.retrieve());


    ANA_CHECK(m_muonSelectionTool_loose.setProperty("MuQuality",2));
    ANA_CHECK(m_muonSelectionTool_loose.setProperty("UseAllAuthors",true));
    ANA_CHECK(m_muonSelectionTool_loose.setProperty("MaxEta",2.5));
    ANA_CHECK(m_muonSelectionTool_loose.retrieve());

    ANA_CHECK(m_muonSelectionTool_medium.setProperty("MuQuality",1));
    ANA_CHECK(m_muonSelectionTool_medium.setProperty("UseAllAuthors",true));
    ANA_CHECK(m_muonSelectionTool_medium.setProperty("MaxEta",2.5));
    ANA_CHECK(m_muonSelectionTool_medium.retrieve());

    ANA_CHECK(m_muonSelectionTool_tight.setProperty("MuQuality",0));
    ANA_CHECK(m_muonSelectionTool_tight.setProperty("UseAllAuthors",true));
    ANA_CHECK(m_muonSelectionTool_tight.setProperty("MaxEta",2.5));
    ANA_CHECK(m_muonSelectionTool_tight.retrieve());

    ANA_CHECK(m_muonSelectionTool_lowpt.setProperty("MuQuality",5));
    ANA_CHECK(m_muonSelectionTool_lowpt.setProperty("UseAllAuthors",true));
    ANA_CHECK(m_muonSelectionTool_lowpt.setProperty("MaxEta",2.5));
    ANA_CHECK(m_muonSelectionTool_lowpt.retrieve());

    ANA_CHECK( m_muonCalibSmearTool.retrieve() );

    ANA_CHECK( m_muonEffSFTightTool.setProperty("WorkingPoint","Tight"));
    ANA_CHECK( m_muonEffSFTightTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyTight"));
    ANA_CHECK( m_muonEffSFTightTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyTight"));
    ANA_CHECK( m_muonEffSFTightTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorTight"));
    ANA_CHECK( m_muonEffSFTightTool.retrieve() );

    ANA_CHECK( m_muonEffSFMediumTool.setProperty("WorkingPoint","Medium"));
    ANA_CHECK( m_muonEffSFMediumTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyMedium"));
    ANA_CHECK( m_muonEffSFMediumTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyMedium"));
    ANA_CHECK( m_muonEffSFMediumTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorMedium"));
    ANA_CHECK( m_muonEffSFMediumTool.retrieve() );

    ANA_CHECK( m_muonEffSFLooseTool .setProperty("WorkingPoint","Loose"));
    ANA_CHECK( m_muonEffSFLooseTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyLoose"));
    ANA_CHECK( m_muonEffSFLooseTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyLoose"));
    ANA_CHECK( m_muonEffSFLooseTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorLoose"));
    ANA_CHECK( m_muonEffSFLooseTool.retrieve() );

    ANA_CHECK( m_muonEffSFLowPtTool.setProperty("WorkingPoint","LowPt"));
    ANA_CHECK( m_muonEffSFLowPtTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyLowPt"));
    ANA_CHECK( m_muonEffSFLowPtTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyLowPt"));
    ANA_CHECK( m_muonEffSFLowPtTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorLowPt"));
    if (m_muonEffSFLowPtTool.retrieve().isFailure()) {
        ATH_MSG_ERROR("LowPt Muon WP reco Eff not yet available!");
    }

    ANA_CHECK( m_muonTriggSFTightTool.setProperty("MuonQuality","Tight"));
    ANA_CHECK( m_muonTriggSFMediumTool.setProperty("MuonQuality","Medium"));
    ANA_CHECK( m_muonTriggSFLooseTool.setProperty("MuonQuality","Loose"));

    // Initialize and configure trigger tools
    ANA_CHECK (m_trigConfigTool.initialize());
    ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool.initialize());


    ANA_CHECK (m_triggerMatchingTool.retrieve());

    ANA_CHECK (m_electon_mediumLH.setProperty("WorkingPoint", "MediumLHElectron"));
    ANA_CHECK (m_electon_tightLH .setProperty("WorkingPoint", "TightLHElectron" ));
    ANA_CHECK (m_electon_looseLH .setProperty("WorkingPoint", "LooseLHElectron" ));

    ANA_CHECK(m_electon_mediumLH.retrieve());
    ANA_CHECK(m_electon_tightLH .retrieve());
    ANA_CHECK(m_electon_looseLH .retrieve());

    ANA_CHECK(m_photon_loose.setProperty("isEMMask",egammaPID::PhotonLoose));
    ANA_CHECK(m_photon_tight.setProperty("isEMMask",egammaPID::PhotonTight));

    ANA_CHECK(m_photon_loose .retrieve());
    ANA_CHECK(m_photon_tight .retrieve());

    return StatusCode::SUCCESS;
}
BLSToolBox::~BLSToolBox()  {}



unsigned int BLSToolBox::runNumber  () const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->runNumber();
}

unsigned int BLSToolBox::eventNumber() const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->eventNumber();
}

unsigned int BLSToolBox::lumiBlock  () const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->lumiBlock();
}

bool     BLSToolBox::isMC()      const {
    return eventInfo()->eventType(xAOD::EventInfo::IS_SIMULATION);
}


const xAOD::EventInfo* BLSToolBox::eventInfo() const {
    const xAOD::EventInfo* evtInfo = 0;
    if (evtStore()->retrieve( evtInfo, "EventInfo" ).isSuccess()) return evtInfo;
    else throw;
}

uint32_t BLSToolBox::timeStamp() const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->timeStamp();
}
uint32_t BLSToolBox::bcid()      const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->bcid();
}
uint32_t BLSToolBox::mcChannelNumber()         const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->mcChannelNumber();
}
unsigned long long BLSToolBox::mcEventNumber() const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->mcEventNumber();
}

float BLSToolBox::actualInteractionsPerCrossing () const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->actualInteractionsPerCrossing();
}
float BLSToolBox::averageInteractionsPerCrossing() const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ) );
    return evtInfo->averageInteractionsPerCrossing();
}

CP::IPileupReweightingTool* BLSToolBox::prwTool(){
    return m_prw_tool.get();
}
const CP::IPileupReweightingTool* BLSToolBox::prwTool() const {
    return m_prw_tool.get();
}



//std::tuple<double,double,double> BLSToolBox::getBeamPos() const {
//    const xAOD::EventInfo* evtInfo = 0;
//    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ));
//
//    return std::make_tuple(evtInfo->beamPosX(),
//                           evtInfo->beamPosY(),
//                           evtInfo->beamPosZ());
//}
StatusCode BLSToolBox::getBeamPos(double &x, double &y, double &z) const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ));
    x = evtInfo->beamPosX();
    y = evtInfo->beamPosY();
    z = evtInfo->beamPosZ();
    return StatusCode::SUCCESS;
}

StatusCode BLSToolBox::getBeamSig(double &x, double &y, double &z) const {
    const xAOD::EventInfo* evtInfo = 0;
    ANA_CHECK( evtStore()->retrieve( evtInfo, "EventInfo" ));
    x = evtInfo->beamPosSigmaX();
    y = evtInfo->beamPosSigmaY();
    z = evtInfo->beamPosSigmaZ();
    return StatusCode::SUCCESS;
}


int BLSToolBox::getYear(uint32_t runNumber) const {
    //if      (runNumber >=  ) return 2018;
    //else
    if      (runNumber >= 348197 && runNumber <= 364485) return 2018;
    else if (runNumber >= 324320 && runNumber <= 341649) return 2017;
    else if (runNumber >= 289496 && runNumber <= 321044) return 2016;
    else if (runNumber >= 251101 && runNumber <= 287931) return 2015;
    else if (runNumber >= 200498 && runNumber <= 216524) return 2012;
    else if (runNumber >= 176003 && runNumber <= 194417) return 2011;
    else if (runNumber >= 148767 && runNumber <= 170484) return 2010;
    else if (runNumber >= 122050 && runNumber <= 142406) return 2009;
    else {
        ATH_MSG_ERROR("No valid year found for run: " << runNumber);
        throw;
    }
    return -1;
}





// ---------------- MUONS --------------------- //
StatusCode BLSToolBox::passMuon      (const xAOD::Muon* muon, xAOD::Muon::Quality qual, bool & isPassed) const {
    switch (qual) {
        case xAOD::Muon::Loose:
        return passMuonLoose(muon,isPassed);
        case xAOD::Muon::Medium:
        return passMuonMedium(muon,isPassed);
        case xAOD::Muon::Tight:
        return passMuonTight(muon,isPassed);
        default:
        ATH_MSG_ERROR("No valid muon quality defined: " << qual);
        return StatusCode::FAILURE;
    }
}
StatusCode BLSToolBox::passMuon      (const xAOD::Muon* muon, BLSQuality qual, bool & isPassed) const {
    switch (qual) {
        case BLSQuality::Tight:
        return passMuonTight(muon,isPassed);
        case BLSQuality::LowPt:
        return passMuonLowPt(muon,isPassed);
        case BLSQuality::Loose:
        return passMuonLoose(muon,isPassed);
        case BLSQuality::Medium:
        return passMuonMedium(muon,isPassed);
        default:
        ATH_MSG_ERROR("No valid BLS Muon quality defined: " << qual);
        return StatusCode::FAILURE;
    }
}


StatusCode BLSToolBox::passMuonLoose (const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_loose->accept(*muon);
    return StatusCode::SUCCESS;
}
StatusCode BLSToolBox::passMuonMedium(const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_medium->accept(*muon);
    return StatusCode::SUCCESS;
}
StatusCode BLSToolBox::passMuonTight (const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_tight->accept(*muon);
    return StatusCode::SUCCESS;
}
StatusCode BLSToolBox::passMuonLowPt (const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_lowpt->accept(*muon);
    return StatusCode::SUCCESS;
}


std::unique_ptr<xAOD::Muon> BLSToolBox::makeCalibMuon(const xAOD::Muon* origMu) const {
    xAOD::Muon* newMuon(nullptr);

    auto corrCode = m_muonCalibSmearTool->correctedCopy(*origMu,newMuon);
    if (corrCode != CP::CorrectionCode::Ok) {
        ATH_MSG_WARNING("Muon smearing gave error: " << corrCode);
    }
    std::unique_ptr<xAOD::Muon> nm(newMuon);
    return nm;
}


const CP::IMuonEfficiencyScaleFactors* BLSToolBox::muonEffSFTightTool() const {
    return m_muonEffSFTightTool.get();
}
const CP::IMuonEfficiencyScaleFactors* BLSToolBox::muonEffSFMediumTool() const {
    return m_muonEffSFMediumTool.get();
}
const CP::IMuonEfficiencyScaleFactors* BLSToolBox::muonEffSFLooseTool() const {
    return m_muonEffSFLooseTool.get();
}
const CP::IMuonEfficiencyScaleFactors* BLSToolBox::muonEffSFLowPtTool() const {
    if (m_muonEffSFLowPtTool.isInitialized()) return m_muonEffSFLowPtTool.get();
    else {
        ATH_MSG_WARNING("m_muonEffSFLowPtTool not initialized; returning nullptr; likely your code will now crash");
        return nullptr;
    }
}

CP::CorrectionCode BLSToolBox::getMCPEfficiency(const CP::IMuonEfficiencyScaleFactors* muEffSFTool, const xAOD::Muon& mu, float& eff, const xAOD::EventInfo* info, bool isMC) const {
	if(!isMC){
		auto corrCodeEff = muEffSFTool->getDataEfficiency(mu, eff, info);
		return corrCodeEff;
	}
	else{
		auto corrCodeEff = muEffSFTool->getMCEfficiency(mu, eff, info);
		return corrCodeEff;
	}
}

const CP::IMuonTriggerScaleFactors* BLSToolBox::muonTriggSFTightTool() const {
    return m_muonTriggSFTightTool.get();
}

const CP::IMuonTriggerScaleFactors* BLSToolBox::muonTriggSFMediumTool() const {
    return m_muonTriggSFMediumTool.get();
}

const CP::IMuonTriggerScaleFactors* BLSToolBox::muonTriggSFLooseTool() const {
    return m_muonTriggSFLooseTool.get();
}

// CONTAINERS
const xAOD::MuonContainer* BLSToolBox::muons() const {
    const xAOD::MuonContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_muons_key ).isFailure()) {
        ATH_MSG_ERROR("No Muon Container called " << m_muons_key);
        throw;
    }
    return cont;
}

const xAOD::TrackParticleContainer* BLSToolBox::tracks   () const {
    const xAOD::TrackParticleContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_tracks_key ).isFailure()) {
        ATH_MSG_ERROR("No Track Container called " << m_tracks_key);
        throw;
    }
    return cont;
}
const xAOD::VertexContainer       * BLSToolBox::pvs      () const {
    const xAOD::VertexContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_pv_key ).isFailure()) {
        ATH_MSG_ERROR("No Vertex Container called " << m_pv_key);
        throw;
    }
    return cont;
}
const xAOD::ElectronContainer     * BLSToolBox::electrons() const {
    const xAOD::ElectronContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_electrons_key ).isFailure()) {
        ATH_MSG_ERROR("No Electron Container called " << m_electrons_key);
        throw;
    }
    return cont;
}
const xAOD::PhotonContainer       * BLSToolBox::photons  () const {
    const xAOD::PhotonContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_photons_key ).isFailure()) {
        ATH_MSG_ERROR("No Photon Container called " << m_photons_key);
        throw;
    }
    return cont;
}
const xAOD::JetContainer          * BLSToolBox::jets     () const {
    return BLSToolBox::jets(m_jets_key);
}
const xAOD::JetContainer          * BLSToolBox::jets     (const std::string & key) const {
    const xAOD::JetContainer* cont = 0;
    if (evtStore()->retrieve( cont, key ).isFailure()) {
        ATH_MSG_ERROR("No Jet Container called " << key);
        throw;
    }
    return cont;
}


const xAOD::VertexContainer       * BLSToolBox::vertices      (const std::string & key) const {
    const xAOD::VertexContainer* cont = 0;
    if (evtStore()->retrieve( cont, key ).isFailure()) {
        ATH_MSG_ERROR("No Vertex Container called " << m_pv_key);
        throw;
    }
    return cont;
}


const xAOD::TruthEventContainer   * BLSToolBox::truthEvents   () const {
    const xAOD::TruthEventContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_truthEvents_key ).isFailure()) {
        ATH_MSG_ERROR("No TruthEvent Container called " << m_truthEvents_key);
        throw;
    }
    return cont;
}
const xAOD::TruthParticleContainer* BLSToolBox::truthParticles() const {
    const xAOD::TruthParticleContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_truthParticles_key ).isFailure()) {
        ATH_MSG_ERROR("No TruthParticleContainer Container called " << m_truthParticles_key);
        throw;
    }
    return cont;

}
const xAOD::TruthVertexContainer  * BLSToolBox::truthVertices () const {
    const xAOD::TruthVertexContainer* cont = 0;
    if (evtStore()->retrieve( cont, m_truthVertices_key ).isFailure()) {
        ATH_MSG_ERROR("No TruthVertexContainer Container called " << m_truthVertices_key);
        throw;
    }
    return cont;
}


const Trig::TrigDecisionTool* BLSToolBox::tdt() const {
    return m_trigDecisionTool.get();
}

bool BLSToolBox::isPassed(const std::string&name) const  {
    auto t = tdt();
    return t->isPassed(name);
}


const Trig::IMatchingTool* BLSToolBox::trigMatchingTool() const {
    return &(*m_triggerMatchingTool);
}
bool BLSToolBox::match(const xAOD::IParticle& recoObject, const std::string& chain, double matchThreshold, bool rerun) {
    return m_triggerMatchingTool->match(recoObject, chain, matchThreshold,rerun);
}
bool BLSToolBox::match(const std::vector<const xAOD::IParticle*>& recoObjects, const std::string& chain, double matchThreshold, bool rerun) {
    return m_triggerMatchingTool->match(recoObjects, chain, matchThreshold,rerun);
}




std::ostream& operator<<(std::ostream& os, BLS::BLSQuality c)
{
    switch(c)
    {
        case BLS::BLSQuality::VeryLoose: os << "VeryLoose";    break;
        case BLS::BLSQuality::Loose    : os << "Loose"; break;
        case BLS::BLSQuality::Medium   : os << "Medium";  break;
        case BLS::BLSQuality::Tight    : os << "Tight";   break;
        case BLS::BLSQuality::LowPt    : os << "LowPt";   break;
        default       : os.setstate(std::ios_base::failbit);
    }
    return os;
}



const std::map<TString, Long64_t>& BLSToolBox::counter() const {
    return m_counter;
}

void BLSToolBox::add_count(const TString &label) {
    ++m_counter[label];
}

void BLSToolBox::dump_counter(const std::string &prefix) const {
    //#FIXME - add section to convert to value-orderd, not key-orderd
    ATH_MSG_INFO(prefix << " Counter");
    for (auto x: m_counter) {
        ATH_MSG_INFO(prefix << ": " << x.first << " " << x.second);
    }
    ATH_MSG_INFO("=================");

}

std::unique_ptr<xAOD::BPhysHelper> BLSToolBox::makeBPhys(const xAOD::Vertex*vtx) const {
    if (!vtx) {
        ATH_MSG_FATAL("NULL vtx pointer given");
        throw;
    }
    auto bphys = std::make_unique<xAOD::BPhysHelper>(vtx);
    return bphys;
}

std::unique_ptr<xAOD::BPhysHypoHelper> BLSToolBox::makeBPhysHypo(const std::string & hypo, const xAOD::Vertex* vtx) const {
    if (!vtx) {
        ATH_MSG_FATAL("NULL vtx pointer given");
        throw;
    }
    auto bphys = std::make_unique<xAOD::BPhysHypoHelper>(hypo,vtx);
    return bphys;
}



Root::TAccept BLSToolBox::isElectronMediumLH(const xAOD::Electron* el) const {
    return m_electon_mediumLH->accept(el);
}
Root::TAccept BLSToolBox::isElectronLooseLH (const xAOD::Electron* el) const {
    return m_electon_looseLH->accept(el);
}
Root::TAccept BLSToolBox::isElectronTightLH (const xAOD::Electron* el) const {
    return m_electon_tightLH->accept(el);
}

Root::TAccept  BLSToolBox::isPhotonLoose (const xAOD::Photon*pho,bool useDerivationValues) const {
    //https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#Object_quality_cut
    if (!useDerivationValues) {
        return m_photon_loose->accept(pho);
    } else {
        if (!pho->isAvailable<char>("DFCommonPhotonsIsEMLoose")) {
            ATH_MSG_FATAL("Variable DFCommonPhotonsIsEMLoose not in derivation");
            throw;
        } else {
            Root::TAccept accept("DFCommonPhotonsIsEMLoose");
            accept.setCutResult(accept.addCut("DFCommonPhotonsIsEMLoose","derivation label"),
                          static_cast<bool>(pho->auxdataConst<char>("DFCommonPhotonsIsEMLoose")));
            return accept;
        }
    }
}

Root::TAccept  BLSToolBox::isPhotonTight (const xAOD::Photon* pho,bool useDerivationValues) const {
    if (!useDerivationValues) {
        return m_photon_tight->accept(pho);
    } else {
        if (!pho->isAvailable<char>("DFCommonPhotonsIsEMTight")) {
            ATH_MSG_FATAL("Variable DFCommonPhotonsIsEMTight not in derivation");
            throw;
        } else {
            Root::TAccept accept("DFCommonPhotonsIsEMTight");
            accept.setCutResult(accept.addCut("DFCommonPhotonsIsEMTight","derivation label"),
                          static_cast<bool>(pho->auxdataConst<char>("DFCommonPhotonsIsEMTight")));
            return accept;
        }
    }

}

bool BLSToolBox::isolation(float& value, const xAOD::IParticle* ptl, xAOD::Iso::IsolationType isoType) const {
    switch (ptl->type()) {
        case xAOD::Type::Photon:
            return (dynamic_cast<const xAOD::Photon*>(ptl))->isolation(value,isoType);
            break;
        case xAOD::Type::Electron:
            return (dynamic_cast<const xAOD::Electron*>(ptl))->isolation(value,isoType);
            break;
        case xAOD::Type::Muon:
            return (dynamic_cast<const xAOD::Muon*>(ptl))->isolation(value,isoType);
            break;

        default:
            ATH_MSG_WARNING("Particle type for Isolation not defined");
    }

    return false; // no possible value
}

bool BLSToolBox::VatoCS(const TLorentzVector & mupl,
                        const TLorentzVector & mumi,
                        const TLorentzVector & gamma,
                        TVector3 & CSAxis,
                        TVector3 & xAxis,
                        TVector3 & yAxis,
                        double & cosTheta_dimu,
                        double & cosTheta_gamma,
                        double & phi_dimu,
                        double & phi_gamma) const {

    TLorentzVector dimu  = mupl + mumi;
    TLorentzVector higgs = dimu + gamma;

    double ProtonMass = 938.272; // MeV
    double BeamEnergy = 6500000.; // MeV

    // 1)
    TLorentzVector p1, p2;
    p1.SetPxPyPzE(0.,0.,BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));
    p2.SetPxPyPzE(0.,0.,-1*BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));

    // 2)
    TLorentzVector dimu_hf, mupl_hf, mumi_hf,gamma_hf, higgs_hf, p1_hf, p2_hf;

    dimu_hf   = dimu ;
    mupl_hf   = mupl ;
    mumi_hf   = mumi ;
    gamma_hf  = gamma;
    higgs_hf  = higgs;
    p1_hf     = p1;
    p2_hf     = p2;

    dimu_hf   .Boost(-higgs.BoostVector());
    mupl_hf   .Boost(-higgs.BoostVector());
    mumi_hf   .Boost(-higgs.BoostVector());
    gamma_hf  .Boost(-higgs.BoostVector());
    higgs_hf  .Boost(-higgs.BoostVector());
    p1_hf     .Boost(-higgs.BoostVector());
    p2_hf     .Boost(-higgs.BoostVector());

    // 3)
    CSAxis = (p1_hf.Vect().Unit()-p2_hf.Vect().Unit()).Unit();
    yAxis  = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
    yAxis  = yAxis.Unit();
    xAxis  = yAxis.Cross(CSAxis);
    xAxis  = xAxis.Unit();



    //4)

    phi_dimu      = atan2((dimu_hf.Vect()*yAxis),(dimu_hf.Vect()*xAxis));
    cosTheta_dimu = dimu_hf.Vect().Dot(CSAxis) / (dimu_hf.Vect().Mag());

    // cross-check
    cosTheta_gamma = gamma_hf.Vect().Dot(CSAxis) / (gamma_hf.Vect().Mag());
    phi_gamma      = atan2((gamma_hf.Vect()*yAxis),(gamma_hf.Vect()*xAxis));


    if (fabs(cosTheta_dimu + cosTheta_gamma) > 0.001) {
        ATH_MSG_WARNING("VatoCS: cosThetaCS sum, beyond tolerance: " << cosTheta_dimu << " " << cosTheta_gamma);
    }
    if (fabs(sin(phi_dimu - phi_gamma)) > 0.001) {
        ATH_MSG_WARNING("VatoCS: phiCS difference, beyond tolerance: " << phi_dimu << " " << phi_gamma);
    }

//    ATH_MSG_INFO("MuPl:");
//    mupl.Print();
//    mupl_hf.Print();
//    ATH_MSG_INFO("MuMi:");
//    mumi.Print();
//    mumi_hf.Print();
//    ATH_MSG_INFO("Dimu:");
//    dimu.Print();
//    dimu_hf.Print();
//    ATH_MSG_INFO("Gamma:");
//    gamma.Print();
//    gamma_hf.Print();
//    ATH_MSG_INFO("Higgs:");
//    higgs.Print();
//    higgs_hf.Print();
//    ATH_MSG_INFO("p1:");
//    p1.Print();
//    p1_hf.Print();
//    ATH_MSG_INFO("p2:");
//    p2.Print();
//    p2_hf.Print();
//    ATH_MSG_INFO("CSAxis:");
//    CSAxis.Print();
//    ATH_MSG_INFO("xAxis:");
//    xAxis.Print();
//    ATH_MSG_INFO("yAxis:");
//    yAxis.Print();
//
//    ATH_MSG_INFO("cosTheta / phi dimu : " << cosTheta_dimu << " " << phi_dimu );
//    ATH_MSG_INFO("cosTheta / phi gamma: " << cosTheta_gamma << " " << phi_gamma );



    return true;
}


bool BLSToolBox::VatoCS(const TLorentzVector & mupl,
                        const TLorentzVector & mumi,
                        TVector3 & CSAxis,
                        TVector3 & xAxis,
                        TVector3 & yAxis,
                        double & cosTheta_mupl,
                        double & cosTheta_mumi,
                        double & phi_mupl,
                        double & phi_mumi) const {

    TLorentzVector dimu  = mupl + mumi;

    double ProtonMass = 938.272; // MeV
    double BeamEnergy = 6500000.; // MeV

    // 1)
    TLorentzVector p1, p2;
    p1.SetPxPyPzE(0.,0.,BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));
    p2.SetPxPyPzE(0.,0.,-1*BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));

    // 2)
    TLorentzVector dimu_hf, mupl_hf, mumi_hf, p1_hf, p2_hf;
    dimu_hf   = dimu ;
    mupl_hf   = mupl ;
    mumi_hf   = mumi ;
    p1_hf     = p1;
    p2_hf     = p2;

    dimu_hf   .Boost(-dimu.BoostVector());
    mupl_hf   .Boost(-dimu.BoostVector());
    mumi_hf   .Boost(-dimu.BoostVector());
    p1_hf     .Boost(-dimu.BoostVector());
    p2_hf     .Boost(-dimu.BoostVector());

    // 3)
    CSAxis = (p1_hf.Vect().Unit()-p2_hf.Vect().Unit()).Unit();
    yAxis  = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
    yAxis  = yAxis.Unit();
    xAxis  = yAxis.Cross(CSAxis);
    xAxis  = xAxis.Unit();

    // 4)
    phi_mupl      = atan2((mupl_hf.Vect()*yAxis),(mupl_hf.Vect()*xAxis));
    cosTheta_mupl = mupl_hf.Vect().Dot(CSAxis) / (mupl_hf.Vect().Mag());

    phi_mumi      = atan2((mumi_hf.Vect()*yAxis),(mumi_hf.Vect()*xAxis));
    cosTheta_mumi = mumi_hf.Vect().Dot(CSAxis) / (mumi_hf.Vect().Mag());

    if (fabs(cosTheta_mupl + cosTheta_mumi) > 0.001) {
        ATH_MSG_WARNING("VatoCS: cosThetaCS sum, beyond tolerance: " << cosTheta_mupl << " " << cosTheta_mumi);
    }
    if (fabs(sin(phi_mupl - phi_mumi)) > 0.001) {
        ATH_MSG_WARNING("VatoCS: phiCS difference, beyond tolerance: " << phi_mupl << " " << phi_mumi);
    }

    return true;
}







bool BLSToolBox::CSFrame(const TLorentzVector & Z, TVector3 &CSAxis,
                     TVector3 &xAxis, TVector3 &yAxis, bool qDirOK,
                     double beamEnergy)const
{

    double ProtonMass = 938.272; // MeV
    double BeamEnergy = beamEnergy; // MeV

    double sign  = fabs(Z.Z())/Z.Z();
    bool isGood = true;
    TLorentzVector p1, p2;

    if (qDirOK){
        p1.SetPxPyPzE(0, 0, sign*BeamEnergy,
                      TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // quark
        p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,
                      TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // antiquark
    } else {
        p1.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,
                      TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // quark
        p2.SetPxPyPzE(0, 0, sign*BeamEnergy,
                      TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // antiquark
    }
    p1.Boost(-Z.BoostVector());
    p2.Boost(-Z.BoostVector());
    CSAxis = (p1.Vect().Unit()-p2.Vect().Unit()).Unit();
    yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
    yAxis = yAxis.Unit();
    xAxis = yAxis.Cross(CSAxis);
    xAxis = xAxis.Unit();

    return isGood;
}


double BLSToolBox::getCosTheta_direct(const TLorentzVector & Z,
                                  const TLorentzVector & muM,
                                  const TLorentzVector & muP, bool qDirOK)const {
    double cosTheta;
    double sign  = fabs(Z.Z())/Z.Z();
    if (!qDirOK)
        sign *= -1;
    double part1 = 2 / ( Z.M()*sqrt(Z.M()*Z.M() + Z.Pt()*Z.Pt() ));
    double part2 = (muM.Plus()*muP.Minus() - muP.Plus()*muM.Minus())/2;
    cosTheta = part1*part2;
    // add sign according to boost direktion in z
    cosTheta *= sign;

    return cosTheta;
} // getCosTheta_direct

double BLSToolBox::getPhi(const TLorentzVector & Z,
                      const TLorentzVector & muM,
                      const TLorentzVector & muP,  bool qDirOK) const
{
    TLorentzVector boostedMu = muM;
    TVector3 CSAxis, xAxis, yAxis;

    boostedMu.Boost(-Z.BoostVector());

    BLSToolBox::CSFrame(Z, CSAxis, xAxis, yAxis, qDirOK);
    double phi = atan2((boostedMu.Vect()*yAxis),(boostedMu.Vect()*xAxis));
    if(phi<0) phi = phi + 2*M_PI;

    return phi;
}


double BLSToolBox::getCosTheta(const TLorentzVector & Z,
                           const TLorentzVector & muM,
                           const TLorentzVector & muP,  bool qDirOK)const
{
    TVector3 CSAxis, yAxis, xAxis;
    TLorentzVector boostedMu = muM;
    boostedMu.Boost(-Z.BoostVector());

    if (!BLSToolBox::CSFrame(Z, CSAxis, xAxis, yAxis, qDirOK))
        return -999;
    return cos(boostedMu.Angle(CSAxis));
}




// from Michael

double BLSToolBox::phiMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const {

    TLorentzVector MuPlus;
    TLorentzVector MuMinus;

    if(Mu1_q > 0){
        MuPlus = Mu1;
        MuMinus = Mu2;
    }
    else{
        MuPlus = Mu2;
        MuMinus = Mu1;
    }


    TLorentzVector MuMu = MuPlus + MuMinus;
    TVector3 V0 = MuMu.Vect();
    MuPlus.RotateZ(-V0.Phi());
    MuPlus.RotateY(-V0.Theta());
    return atan2(MuPlus.Y(),MuPlus.X());
}


double BLSToolBox::cosMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const {
    TLorentzVector MuPlus;
    TLorentzVector MuMinus;

    if(Mu1_q > 0){
        MuPlus = Mu1;
        MuMinus = Mu2;
    }
    else{
        MuPlus = Mu2;
        MuMinus = Mu1;
    }


    TLorentzVector MuMu = MuPlus + MuMinus;

    double MuMu_mass = MuMu.M();
    double MuPlus_mass = MuPlus.M();
    double MuMinus_mass = MuMinus.M();
    double P_MuMu = MuMu.Rho();

    double pssq = ((MuMu_mass*MuMu_mass) - (MuPlus_mass+MuMinus_mass)*(MuPlus_mass+MuMinus_mass)) *  ((MuMu_mass*MuMu_mass) - (MuPlus_mass-MuMinus_mass)*(MuPlus_mass-MuMinus_mass));

    double ps = (pssq>0.) ? sqrt(pssq) : 0.;

    ps/=2.*MuMu_mass;

    double pp = MuMu.Px()*MuPlus.Px() + MuMu.Py()*MuPlus.Py() + MuMu.Pz()*MuPlus.Pz();

    double cosTheta = (MuMu.Energy()*pp - P_MuMu*P_MuMu*MuPlus.Energy())/(MuMu_mass*ps*P_MuMu);


    return cosTheta;

}


bool BLSToolBox::VatoHelicity(const TLorentzVector & mupl,
                             const TLorentzVector & mumi,
                             TVector3 & HelicityAxis,
                             TVector3 & xAxis,
                             TVector3 & yAxis,
                             double & cosTheta_mupl,
                             double & cosTheta_mumi,
                             double & phi_mupl,
                             double & phi_mumi) const {

    TLorentzVector dimu  = mupl + mumi;

    double ProtonMass = 938.272; // MeV
    double BeamEnergy = 6500000.; // MeV

    // 1)
    TLorentzVector p1, p2;
    p1.SetPxPyPzE(0.,0.,BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));
    p2.SetPxPyPzE(0.,0.,-1*BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));

    // 2)
    TLorentzVector dimu_hf, mupl_hf, mumi_hf, p1_hf, p2_hf;
    dimu_hf   = dimu ;
    mupl_hf   = mupl ;
    mumi_hf   = mumi ;
    p1_hf     = p1;
    p2_hf     = p2;

    dimu_hf   .Boost(-dimu.BoostVector());
    mupl_hf   .Boost(-dimu.BoostVector());
    mumi_hf   .Boost(-dimu.BoostVector());
    p1_hf     .Boost(-dimu.BoostVector());
    p2_hf     .Boost(-dimu.BoostVector());

    // 3)
    HelicityAxis = dimu.Vect().Unit();
    yAxis        = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
    yAxis        = yAxis.Unit();
    xAxis        = yAxis.Cross(HelicityAxis);
    xAxis        = xAxis.Unit();

    // 4)
    phi_mupl      = atan2((mupl_hf.Vect()*yAxis),(mupl_hf.Vect()*xAxis));
    cosTheta_mupl = mupl_hf.Vect().Dot(HelicityAxis) / (mupl_hf.Vect().Mag());

    phi_mumi      = atan2((mumi_hf.Vect()*yAxis),(mumi_hf.Vect()*xAxis));
    cosTheta_mumi = mumi_hf.Vect().Dot(HelicityAxis) / (mumi_hf.Vect().Mag());


    //    ATH_MSG_INFO("MuPl:");
    //    mupl.Print();
    //    mupl_hf.Print();
    //    ATH_MSG_INFO("MuMi:");
    //    mumi.Print();
    //    mumi_hf.Print();
    //    ATH_MSG_INFO("Dimu:");
    //    dimu.Print();
    //    dimu_hf.Print();
    //    ATH_MSG_INFO("p1:");
    //    p1.Print();
    //    p1_hf.Print();
    //    ATH_MSG_INFO("p2:");
    //    p2.Print();
    //    p2_hf.Print();
    //    ATH_MSG_INFO("HelicityAxis:");
    //    HelicityAxis.Print();
    //    ATH_MSG_INFO("xAxis:");
    //    xAxis.Print();
    //    ATH_MSG_INFO("yAxis:");
    //    yAxis.Print();

    ATH_MSG_VERBOSE("cosTheta / phi mupl : " << cosTheta_mupl << " " << phi_mupl );
    ATH_MSG_VERBOSE("cosTheta / phi mumi : " << cosTheta_mumi << " " << phi_mumi );



    if (fabs(cosTheta_mupl + cosTheta_mumi) > 0.001) {
        ATH_MSG_WARNING("VatoHelicty: cosTheta sum, beyond tolerance: " << cosTheta_mupl << " " << cosTheta_mumi);
    }
    if (fabs(sin(phi_mupl - phi_mumi)) > 0.001) {
        ATH_MSG_WARNING("VatoHelicty: phi difference, beyond tolerance: " << phi_mupl << " " << phi_mumi);
    }

    return true;
}



bool BLSToolBox::VatoHelicity(const TLorentzVector & mupl, const TLorentzVector & mumi,
                  double & cosTheta_mupl,double & phi_mupl) const {


    TVector3 HelicityAxis;
    TVector3 xAxis;
    TVector3 yAxis;
    double cosTheta_mumi;
    double phi_mumi;

    return BLSToolBox::VatoHelicity(mupl,
                                    mumi,
                                    HelicityAxis,
                                    xAxis,
                                    yAxis,
                                    cosTheta_mupl,
                                    cosTheta_mumi,
                                    phi_mupl,
                                    phi_mumi);
}

bool BLSToolBox::VatoCS(const TLorentzVector & mupl, const TLorentzVector & mumi ,
                        double & cosTheta_mupl , double &phi_mupl ) const {

    TVector3 CSaxis;
    TVector3 xAxis;
    TVector3 yAxis;
    double cosTheta_mumi;
    double phi_mumi;

    return BLSToolBox::VatoCS(mupl,
                              mumi,
                              CSaxis,
                              xAxis,
                              yAxis,
                              cosTheta_mupl,
                              cosTheta_mumi,
                              phi_mupl,
                              phi_mumi);
}

bool BLSToolBox::VatoCS(const TLorentzVector & mupl, const TLorentzVector & mumi,
                        const TLorentzVector & gamma,
                        double & cosThetaCS_dimu,double & phiCS_dimu ) const {

    TVector3 CSaxis;
    TVector3 xAxis;
    TVector3 yAxis;
    double cosTheta_gamma;
    double phi_gamma;

    return BLSToolBox::VatoCS(mupl,
                              mumi,
                              gamma,
                              CSaxis,
                              xAxis,
                              yAxis,
                              cosThetaCS_dimu,
                              cosTheta_gamma,
                              phiCS_dimu,
                              phi_gamma);
}







const xAOD::TruthParticle * BLSToolBox::productionParticle(const xAOD::TruthParticle* particle) const {
    if (!particle) return particle;
    auto prodVtx = particle->prodVtx();
    if (!prodVtx) return particle;

    const int particlePdg(particle->pdgId());

    for ( auto ptr: prodVtx->incomingParticleLinks()) {
        if (!ptr.isValid()) continue;
        if ( (*ptr)->pdgId() != particlePdg) continue;
        // has same pdg, lets see if parent has more
        const xAOD::TruthParticle* p = *ptr;
        return productionParticle(p);
        // note, only looks for the first parent of same pdgID
    } // for

    return particle; // no reason to be here.

} // productionParticle

const xAOD::TruthParticle * BLSToolBox::decayParticle(const xAOD::TruthParticle* particle) const {
    if (!particle) return particle;
    auto decayVtx = particle->decayVtx();
    if (!decayVtx) return particle;

    const int particlePdg(particle->pdgId());

    for ( auto ptr: decayVtx->outgoingParticleLinks()) {
        if (!ptr.isValid()) continue;
        if ( (*ptr)->pdgId() != particlePdg) continue;
        // has same pdg, lets see if parent has more
        const xAOD::TruthParticle* p = *ptr;
        return decayParticle(p);
        // note, only looks for the first parent of same pdgID
    } // for

    return particle; // no good reason to be here.

} // decayParticle

const xAOD::TruthParticle * BLSToolBox::getLastBhadron(const xAOD::TruthParticle*particle) const {
    if (!particle) return particle;

    const xAOD::TruthParticle * ptl = decayParticle(particle);
    auto decayVtx = ptl->decayVtx();
    if (!decayVtx) return ptl;

    for ( auto ptr: decayVtx->outgoingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        if (!p->isBottomHadron()) continue;
        // if here then we have another b-hadron
        return getLastBhadron(p);
    }

    return ptl;

} // getLastBhadron


bool BLSToolBox::hasChildPdg (const xAOD::TruthParticle* mother, int pdgID, bool useAbs) const {
    if (!mother) return false;

    int pdgid = mother->pdgId();
    if (useAbs) {
        if (abs(pdgid) == abs(pdgID)) return true;
    } else {
        if (pdgid == pdgID) return true;
    }

    auto decayVtx = mother->decayVtx();
    if (!decayVtx) return false;

    for ( auto ptr: decayVtx->outgoingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        bool tpchild =  hasChildPdg(p,pdgID,useAbs);
        if (tpchild) return true; // if found

    }
    return false;
}

bool BLSToolBox::hasParentPdg(const xAOD::TruthParticle* child, int pdgID, bool useAbs) const {
    if (!child) return false;

    int pdgid = child->pdgId();
    if (useAbs) {
        if (abs(pdgid) == abs(pdgID)) return true;
    } else {
        if (pdgid == pdgID) return true;
    }

    auto prodVtx = child->prodVtx();
    if (!prodVtx) return false;

    for ( auto ptr: prodVtx->incomingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        bool tp =  hasParentPdg(p,pdgID,useAbs);
        if (tp) return true; // if found

    }
    return false;
}


const xAOD::TruthParticle* BLSToolBox::getDescendentPdg (const xAOD::TruthParticle* mother, int pdgID, bool useAbs) const {
    if (!mother) return nullptr;

    int pdgid = mother->pdgId();
    if (useAbs) {
        if (abs(pdgid) == abs(pdgID)) return mother;
    } else {
        if (pdgid == pdgID) return mother;
    }

    auto decayVtx = mother->decayVtx();
    if (!decayVtx) return nullptr;

    for ( auto ptr: decayVtx->outgoingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        auto tpchild =  getDescendentPdg(p,pdgID,useAbs);
        if (tpchild) return tpchild; // if found
    }
    return nullptr;
}


bool BLSToolBox::getChildren(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> & children,
                 int pdgID, bool useAbs) const {

    if (!mother) return false;
    int pdgid = mother->pdgId();
    if (useAbs) {
        if (abs(pdgid) == abs(pdgID)) children.push_back(mother);
    } else {
        if (pdgid == pdgID) children.push_back(mother);
    }

    auto decayVtx = mother->decayVtx();
    if (!decayVtx) return false;

    for ( auto ptr: decayVtx->outgoingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        getChildren(p,children,pdgID,useAbs); // if not found here, look for childrens'
    }
    return children.size() > 0;
}

bool BLSToolBox::getParents(const xAOD::TruthParticle* child, std::vector<const xAOD::TruthParticle*> & parents,
                int pdgID, bool useAbs) const {
    if (!child) return false;
    int pdgid = child->pdgId();
    if (useAbs) {
        if (abs(pdgid) == abs(pdgID)) parents.push_back(child);
    } else {
        if (pdgid == pdgID) parents.push_back(child);
    }

    auto prodVtx = child->prodVtx();
    if (!prodVtx) return false;

    for ( auto ptr: prodVtx->incomingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        getParents(p,parents,pdgID,useAbs); // if not found here, look for parents'
    }
    return parents.size() > 0;
}

bool BLSToolBox::getSiblings(const xAOD::TruthParticle* child,std::vector<const xAOD::TruthParticle*> & siblings) const {
    if (!child) return false;
    int pdgid = child->pdgId();

    auto prodChild = productionParticle(child);
    auto prodVtx   = prodChild->prodVtx();
    if (!prodVtx) return false;

    std::vector<const xAOD::TruthParticle*> found_siblings;
    // get parents
    for ( auto ptr: prodVtx->incomingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = decayParticle(*ptr);
        // and now get the children
        for (auto qLink: p->decayVtx()->outgoingParticleLinks()) {
            if (!qLink.isValid()) continue;
            if (*qLink == prodChild) continue;
            if (std::find(found_siblings.begin(), found_siblings.end(), *qLink)
                != found_siblings.end()) continue;
            found_siblings.push_back(*qLink);
            std::sort(found_siblings.begin(),found_siblings.end());
        } // for
    } // for

    siblings = found_siblings;

    return (siblings.size() > 0);
}


void BLSToolBox::stableChildren(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> & children,
                    double minPt, double maxEta) const {
    if (!mother) return;

    auto decayVtx = mother->decayVtx();

    bool status1 = (mother->status() == 1);
    bool goodBarcode = mother->barcode() < 100000;
    //        std::cout << "JW: " << status1 << (decayVtx==nullptr) << std::endl;
    //        bool sameChild(false);
    //        if (decayVtx) {
    //            sameChild = hasChildPdg(mother, mother->pdgId());
    //        }
    //        std::cout << "JW2: " << status1 << (decayVtx==nullptr)<<sameChild << " " << mother->pdgId()
    //        <<std::endl;
    bool passes_cuts(true);
    if (mother->pt()  < minPt) passes_cuts = false;
    if (fabs(mother->eta()) > maxEta) passes_cuts = false;
    bool isCharged(true);
    if (mother->isNeutrino()) isCharged = false;
    if (mother->isPhoton())   isCharged = false;
    if (mother->isNeutral())  isCharged = false;

    if (passes_cuts && status1 && goodBarcode && isCharged && ! decayVtx) {
        // if status 1 and no children is stable.
        // #FIXME double check this
        children.push_back(mother);
    }

    if (!decayVtx) return;

    for ( auto ptr: decayVtx->outgoingParticleLinks()) {
        if (!ptr.isValid()) continue;
        const xAOD::TruthParticle* p = *ptr;
        stableChildren(p,children,minPt,maxEta); // if not found here, look for childrens'
    }


} // stableChildren

int BLSToolBox::getBflavour(const xAOD::TruthParticle* mother) const{
    if (!mother) return 0;
    // defined as b-bar = +1, b = -1; e.g. B+ => +1; Bs -1

    int pdg = mother->pdgId();
    bool has_b = mother->hasBottom();
    if (!has_b) return 0;

    int q = 0;
    if (mother->isMeson()) {
        if (mother->charge() > 0) q = +1;
        else if (mother->charge() < 0) q = -1;
        else if (pdg > 0) q = -1;
        else if (pdg < 0) q = +1;
        else q = -999;
    } else {
        q = -888; // not implemented yet
    }

    return q;
} // getBflavour



