#Skeleton joboption for a simple analysis job

#---- Minimal joboptions -------

theApp.EvtMax=10                                         #says how many events to run over. Set to -1 for all events
#jps.AthenaCommonFlags.FilesInput = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/DAOD_PHYSVAL/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSVAL.e5458_s3126_r9364_r9315_AthDerivation-21.2.1.0.root"]   #insert your list of input files here (do this before next lines)
jps.AthenaCommonFlags.FilesInput = ["DAOD_BPHY1.11512525._000086.pool.root.1"]

#Now choose your read mode (POOL, xAOD, or TTree):

#POOL:
#import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of any POOL files (but POOL is slow)

#xAOD:
import AthenaRootComps.ReadAthenaxAODHybrid               #FAST xAOD reading!



from PyUtils import AthFile
from collections import namedtuple
MetaTuple = namedtuple('MetaTuple',['project_name','AMITag','triggerStreamOfFile','beam_type','AtlasRelease','stream_names','IS_DATA'])
if len(jps.AthenaCommonFlags.FilesInput()):
    af = AthFile.fopen(jps.AthenaCommonFlags.FilesInput()[0])
    d_d = af.fileinfos
    print True if 'IS_DATA' in d_d['evt_type'] else False
    metainfo = MetaTuple(project_name        =  d_d['tag_info']['project_name'],
                         AMITag              =  d_d['tag_info']['AMITag'],
                         triggerStreamOfFile =  d_d['tag_info']['triggerStreamOfFile'],
                         beam_type           =  d_d['tag_info']['beam_type'],
                         AtlasRelease        =  d_d['tag_info']['AtlasRelease'],
                         stream_names        =  d_d['stream_names'],
                         IS_DATA             =  (True if 'IS_DATA' in d_d['evt_type'] else False))
    print metainfo
else:
    metainfo = MetaTuple(project_name        = None , 
                         AMITag              = None ,
                         triggerStreamOfFile = None ,
                         beam_type           = None ,
                         AtlasRelease        = None ,
                         stream_names        = None ,
                         IS_DATA             = None )



    


#TTree:
#import AthenaRootComps.ReadAthenaRoot                    #read a flat TTree, very fast, but no EDM objects
#svcMgr.EventSelector.TupleName="MyTree"                  #You usually must specify the name of the tree (default: CollectionTree)

ToolSvc += CfgMgr.BLS__BLSToolBox(name="ToolBox",OutputLevel=DEBUG)


algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer
algseq += CfgMgr.BLSToolBoxAlg(ToolBox=ToolSvc.ToolBox)                                 #adds an instance of your alg to it

#-------------------------------


#note that if you edit the input files after this point you need to pass the new files to the EventSelector:
#like this: svcMgr.EventSelector.InputCollections = ["new","list"] 





##--------------------------------------------------------------------
## This section shows up to set up a HistSvc output stream for outputing histograms and trees
## See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_output_trees_and_histogra for more details and examples

#if not hasattr(svcMgr, 'THistSvc'): svcMgr += CfgMgr.THistSvc() #only add the histogram service if not already present (will be the case in this jobo)
#svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"] #add an output root file stream

##--------------------------------------------------------------------


##--------------------------------------------------------------------
## The lines below are an example of how to create an output xAOD
## See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_create_an_output_xAOD for more details and examples

#from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
#xaodStream = MSMgr.NewPoolRootStream( "StreamXAOD", "xAOD.out.root" )

##EXAMPLE OF BASIC ADDITION OF EVENT AND METADATA ITEMS
##AddItem and AddMetaDataItem methods accept either string or list of strings
#xaodStream.AddItem( ["xAOD::JetContainer#*","xAOD::JetAuxContainer#*"] ) #Keeps all JetContainers (and their aux stores)
#xaodStream.AddMetaDataItem( ["xAOD::TriggerMenuContainer#*","xAOD::TriggerMenuAuxContainer#*"] )
#ToolSvc += CfgMgr.xAODMaker__TriggerMenuMetaDataTool("TriggerMenuMetaDataTool") #MetaDataItems needs their corresponding MetaDataTool
#svcMgr.MetaDataSvc.MetaDataTools += [ ToolSvc.TriggerMenuMetaDataTool ] #Add the tool to the MetaDataSvc to ensure it is loaded

##EXAMPLE OF SLIMMING (keeping parts of the aux store)
#xaodStream.AddItem( ["xAOD::ElectronContainer#Electrons","xAOD::ElectronAuxContainer#ElectronsAux.pt.eta.phi"] ) #example of slimming: only keep pt,eta,phi auxdata of electrons

##EXAMPLE OF SKIMMING (keeping specific events)
#xaodStream.AddAcceptAlgs( "BLSToolBoxAlg" ) #will only keep events where 'setFilterPassed(false)' has NOT been called in the given algorithm

##--------------------------------------------------------------------


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

